#include <t2lrcfg>
#include <sm_jail_redie>
#include <sdktools>


bool g_rndstart;
bool given;
int g_iLast;
//int g_iOffset_Armor = -1;

Handle h_Timer_EnableLastCT;

public Plugin myinfo =
{
 name = "Last CT",
 author = "ShaRen",
 description = "Дает преимущества последнему КТ если много Т",
 version = "1.0.1"
};

public void OnPluginStart()
{
	HookEvent("player_death", eV_player_death, EventHookMode_Pre);
	HookEvent("player_disconnect", eV_player_disconnect, EventHookMode_Pre);
	HookEvent("round_start", eV_round_start, EventHookMode_PostNoCopy);
	//g_iOffset_Armor = FindSendPropInfo("CCSPlayer", "m_ArmorValue");
	//HookEvent("round_start", eV_RoundStart);
}

public Action eV_round_start(Handle event, const char[] name, bool DB)
{
	//PrintToChatAll("eV_round_start");
	g_rndstart = true;
	if (h_Timer_EnableLastCT != INVALID_HANDLE) {
		KillTimer(h_Timer_EnableLastCT);
		h_Timer_EnableLastCT = INVALID_HANDLE;
	}
	//PrintToChatAll("eV_round_start");
	Reset();
	//PrintToChatAll("eV_round_start");

	h_Timer_EnableLastCT = CreateTimer(30.0, Timer_EnableLastCT);			// время с начала раунда когда функцией нельзя ползоваться
	//PrintToChatAll("eV_round_start");
	given = false;
	g_iLast = 0;
}

public void t2lrcfg_OnLrStarted()
{
	if (h_Timer_EnableLastCT != INVALID_HANDLE) {
		KillTimer(h_Timer_EnableLastCT);
		h_Timer_EnableLastCT = INVALID_HANDLE;
	}
	Reset();
	g_iLast = 0;
}

void Reset()
{
	for(int i=1; i<MAXPLAYERS; i++)
		if(IsClientConnected(i) && IsClientInGame(i)) {
			SetEntPropFloat(i, Prop_Data, "m_flLaggedMovementValue", 1.0);
			SetEntityGravity(i, 1.0);
	}
}

public Action Timer_EnableLastCT(Handle timer)
{
	g_rndstart = false;									// по истечении n сек с начала раунда
	h_Timer_EnableLastCT = INVALID_HANDLE;
	//PrintToChatAll("Timer_EnableLastCT");
	CheckLastCT();
}

public Action eV_player_disconnect (Handle event, const char[] name, bool dontBroadcast)
{
	int iClient = GetClientOfUserId(GetEventInt(event, "userid"));
	if (g_iLast == iClient)
		g_iLast = 0;
}

public Action eV_player_death (Handle event, const char[] name, bool dontBroadcast)
{
	//int attacker = GetClientOfUserId(GetEventInt(event, "attacker"));
	int iClient = GetClientOfUserId(GetEventInt(event, "userid"));
	SetEntPropFloat(iClient, Prop_Data, "m_flLaggedMovementValue", 1.0);
	if (g_iLast == iClient)
		g_iLast = 0;
	if (IsLrActivated())
		return Plugin_Continue;
	int TCount;
	for(int i = 0; i < MAXPLAYERS +1; i++)
		if (IsValidT(i))
			TCount++;
	if (GetClientTeam(iClient) == 2 && IsValidCT(g_iLast))
		SetEntPropFloat(g_iLast, Prop_Data, "m_flLaggedMovementValue", 1.0 + TCount*0.03);		// убавляем скорость с каждой смертью Т
	if (GetClientTeam(iClient) != 3)			// если умирает не КТ то пропускаем
		return Plugin_Continue;

	CheckLastCT();
	return Plugin_Continue;
}

void CheckLastCT()
{
	int CTCount, TCount;
	if (!IsLrActivated() && !g_iLast) {
		//PrintToChatAll("[Дебаг] Смерть не во время ЛР");
		for(int i = 0; i < MAXPLAYERS +1; i++) {
			if (IsValidCT(i)) {
				//PrintToChatAll("%i", i);
				CTCount++;
				g_iLast = i;
			}
			if (IsValidT(i))
				TCount++;
		}
	}
	//PrintToChatAll("CheckLastCT g_iLast = %N", g_iLast);
	//PrintToChatAll(given ? "given true":"given false");
	if ( CTCount == 1 && !given) {
		//PrintToChatAll("[Дебаг] Остался последний живой КТ - %N", g_iLast);
		if (!g_rndstart) {
			SetEntityHealth(g_iLast, GetClientHealth(g_iLast) + TCount*10);
			//PrintToChatAll("A + %i", TCount*20);
			//PrintToChatAll("A %i", GetClientArmor(g_iLast));
			//PrintToChatAll("Offset_A %i", g_iOffset_Armor);
			//SetEntData(g_iLast, g_iOffset_Armor, GetClientArmor(g_iLast) + TCount*20);
			//PrintToChatAll("A %i", GetClientArmor(g_iLast));

			SetEntPropFloat(g_iLast, Prop_Data, "m_flLaggedMovementValue", 1.0 + TCount*0.03);
			if (TCount > 6) {
				GivePlayerItem(g_iLast, "item_kevlar");
				for(int i; i<4;i++)
					GivePlayerItem(g_iLast, "weapon_healthshot");
			}
			PrintToChatAll("последний КТ - %N получил доп. +%i хп, броню, 4 аптечки и скорость +%.2f%%", g_iLast, TCount*10, TCount*0.03*100);
			given = true;
		} //else PrintToChatAll("[Дебаг] последний КТ - %N не получил хп", g_iLast);
	} else g_iLast = 0;
	//PrintToChatAll("[Дебаг] %N, живых CT - %i, живых T - %i", g_iLast, CTCount, TCount);
}

stock bool IsValidCT(int client)
{
	//if (client <= 0 || client >= 65 || !IsClientInGame(client))		// || !IsPlayerAlive(client) || GetClientTeam(client) != 3 || IsFakeClient(client)
	if (client <= 0 || client >= 65 || !IsClientInGame(client) || !IsPlayerAlive(client) || IsPlayerGhost(client) || GetClientTeam(client) != 3)
		return false;
	return true;
}

stock bool IsValidT(int client)
{
	if (client <= 0 || client >= 65 || !IsClientInGame(client) || !IsPlayerAlive(client) || IsPlayerGhost(client) || GetClientTeam(client) != 2)
		return false;
	return true;
}